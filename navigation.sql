DROP table if exists navigation;
CREATE TABLE navigation(
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
   `pid` int(11) unsigned,
   `name` CHAR(16),
   `link` CHAR(32),
   PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- kelkite pagrindinius meniu punktus į DB:

-- ◦ Darbuotojai,
INSERT INTO navigation(id, pid, name, link) VALUES(1, 0, 'Darbuotojai', 'darbuotojai.php');
-- ◦ Projektai, projektai.php
INSERT INTO navigation(id, pid, name, link) VALUES(2, 0, 'Projektai', 'projektai.php');
-- ◦ Statistika,
INSERT INTO navigation(id, pid, name, link) VALUES(3, 0, 'Statistika', 'statistika.php');


-- Sukelkite sub punktus prie punkto darbuotojai (galite naudoti ir savo pavadinimus):

-- ◦ Darbuotojų sąrašas, darbuotojai_list.php
INSERT INTO navigation( pid,`name`, link) VALUES(1, 'Sarasas', 'darbuotoju_sarasas.php');

-- ◦ Darbo užkokesčiai, darbuotojai_uzmokesciai.php
INSERT INTO navigation( pid, `name`, link) VALUES(1, 'uzmokesciai', 'darbuotoju_uzmokesciai.php');



-- Sukelkite sub punktus prie punkto Statistika (galite naudoti ir savo pavadinimus):

-- ◦ Projektų statistika, statistika_projektai.php
INSERT INTO navigation( pid, `name`, link) VALUES(3, 'Projektu', 'projektu_statistika.php');

-- ◦ Darbuotojų statistika, statistika_darbuotojai.php
INSERT INTO navigation( pid, `name`, link) VALUES(3, 'Darbuotoju', 'darbuotoju_statistika.php');

show create database baltic_talents;

show create table darbuotojai;