<?php

require_once 'functions.php';
$pdo = getConnection();

$navigation = getNavigation($pdo, 0);

printNavigation($pdo, $navigation);

function printNavigation($pdo, $navigation)
{
    echo '<ul>';
    foreach ($navigation as $navigationItem) {
        echo '<li>' . $navigationItem['name'] . '</li>';
        $children = getNavigation($pdo, $navigationItem['id']);
        if (count($children)) {
            printNavigation($pdo, $children);
        }
    }
    echo '</ul>';
}

// echo '<ul>';
// foreach ($navigation as $navigationItem) {
//     echo '<li>' . $navigationItem['name'] . '</li>';
//     $children = getNavigation($pdo, $navigationItem['id']);
//     if (count($children)) {
//         echo '<ul>';
//         foreach ($children as $childItem) {
//             echo '<li>' . $childItem['name'] . '</li>';
//         }

//         echo '</ul>';
//     }
// }

// echo '</ul>';
//
