<?php
const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DATABASE = 'baltic_talents';

function getConnection(): PDO
{
    $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    return $pdo;
}

function getNavigation(Pdo $pdo, int $parentId)
{
    $sql = "SELECT id, pid, name, link FROM navigation WHERE pid=:pid";
    $query = $pdo->prepare($sql);
    $query->execute(['pid' => $parentId]);

    return $query->fetchAll();
}
